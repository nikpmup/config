" Autoload vimrc
set shell=/bin/bash
set exrc
set secure

" UI
syntax on
set number
set ruler
set colorcolumn=80
set cursorline
set backspace=indent,eol,start

if has("autocmd")
    filetype plugin indent on
    autocmd FileType make set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab
endif

" Tab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Search
set smartcase
set hlsearch
set incsearch
set ignorecase

" View whitespace
set listchars=eol:⏎,tab:→ ,trail:·,space:·
set list

" Random
set guicursor=
autocmd OptionSet guicursor noautocmd set guicursor=

" Keymapping
imap jj <ESC>
imap ;; <ESC>$a;<ESC>
nmap ;; <ESC>$a;<ESC>
nmap [ :tp<CR>
nmap ] :tn<CR>

" Vundle stuff
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle')
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" General
Plugin 'gmarik/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'jiangmiao/auto-pairs'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'honza/vim-snippets'
Plugin 'rhysd/vim-clang-format'
Plugin 'tpope/vim-fugitive'
Plugin 'tomasr/molokai'

" C++/OpenGL
Plugin 'Mizuchi/STL-Syntax'
Plugin 'tikhomirov/vim-glsl'
Plugin 'beyondmarc/opengl.vim'
Plugin 'SirVer/ultisnips'
Plugin 'git@bitbucket.org:nikpmup/vim-cpp-snippets.git'

" Fish
Plugin 'dag/vim-fish'

" Work
let AMZ = $HOME."/.vimrc_amz"
if !empty(glob(AMZ))
    source ~/.vimrc_amz
endif

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" YouCompleteMe Settings
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'
let g:ycm_complete_in_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1

" Clang Format Settings
let g:clang_format#code_style = "google"
autocmd FileType c,cpp,h,hpp map <C-i> :ClangFormat<CR>

" Syntastic Settings
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_auto_refresh_includes = 1
let g:syntastic_cpp_compiler_options = '-std=c++0x'
let g:syntastic_cpp_compiler = 'clang++'

" NerdTree Settings
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" Whitespace
map <C-\> :StripWhitespace<CR>

" Ultisnips
let g:UltiSnipsExpandTrigger="<c-j>"

" File mapping
au BufNewFile,BufRead *.inline set filetype=cpp
au BufNewFile,BufRead *.aidl set filetype=java

" Molokai colorscheme
colorscheme molokai
