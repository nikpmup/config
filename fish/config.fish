# Hostname
set -x PS1 "\u@Pancake:\w\$ "

# Navigation
function u
    # check how many times to go up
    if [ "$argv" = "" ]
        set up "1"
    else
        set up $argv
    end

    for x in (seq $up)
        cd ..
    end
end

function ls
    /bin/ls -G $argv
end

function ll
    /bin/ls -Gla $argv
end

function vim
    nvim $argv
end

# Ported fasd_cd
function fasd_cd
    if [ (count $argv) -le 1 ]
        fasd $argv
    else
        set -l _fasd_ret (fasd -e 'printf %s' $argv)
        [ -z $_fasd_ret ]; and return
        [ -d $_fasd_ret ]; and cd $_fasd_ret; or printf "%s\n" $_fasd_ret
    end
end

function _fasd_preexec -e fish_preexec
    fasd --proc $argv > /dev/null 2>&1
end

alias z 'fasd_cd -d'

# Private Config
source $HOME/.config/fish/private.fish
